# ref https://g.co/gemini/share/84075acd3427
# ref https://stackoverflow.com/a/55240457/248616
from flask import Flask, render_template

from service.mongo_service import get_list_todo, insert_todo, update_todo

app = Flask(__name__)

@app.route('/')
def index():
    return str(get_list_todo())


@app.route('/insert')
def insert():
    return insert_todo()


@app.route('/update/<id>')
def update(id):
    update_data = {'$set': {'title': 'Learn Flask with Mongo', 'is_completed': False}}
    return update_todo(id, update_data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
