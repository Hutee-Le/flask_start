from pymongo import MongoClient

#region init mongo db
MONGO_DB_HOST  = 'localhost'
MONGO_DB_PORT  = 44445  # container up by db\docker-compose.yml
mongodb_client = MongoClient(f'mongodb://root:root@{MONGO_DB_HOST}:{MONGO_DB_PORT}')

mgdb  = mongodb_client.todo_db
todos = mgdb.todo_collection
#endregion init mongo db

# inserted doc
def insert_todo(filter:dict):
  inserted_doc = mgdb.todo_collection.insert_one(filter)
  print(inserted_doc)
  return inserted_doc

# find all doc in collection
def list_all():
  cursor = mgdb.todo_collection.find({})
  l = list(cursor)
  print(l)

#edit doc
def update_todo(_id, update_data):
  update_result = mgdb.todo_collection.update_one({'_id': _id}, update_data)
  print(update_result)

# delete inserted doc
def delete_todo(_id):
  delete_result = mgdb.todo_collection.delete_one({'_id': _id})
  print(delete_result)
