from service.mongo_service import mgdb, list_all, delete_todo, insert_todo, update_todo

inserted_doc = insert_todo({
  'title': 'Alice',
  'is_completed': True,
})

list_all()

update_todo(inserted_doc.inserted_id, {'$set': {'title': 'Learn Flask with Mongo', 'is_completed': False}})

list_all()

delete_todo(inserted_doc.inserted_id)
