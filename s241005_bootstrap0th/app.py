# ref https://g.co/gemini/share/84075acd3427
# ref https://stackoverflow.com/a/55240457/248616
from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index2():
    return render_template('index.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
