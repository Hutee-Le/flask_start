inventory = {}
while True:
    action = input('Enter (a)dd, (u)pdate, (r)emove, or (s)how: ').lower()
    if action == 'q':
        break

    if action == 'a':
        id = input('Enter product ID: ')
        quantity = int(input('Enter quantity: '))
        inventory[id] = quantity
    elif action == 'u':
        id = input('Enter product ID to update: ')
        if id in inventory:
            quantity = int(input('Enter new quantity: '))
            inventory[id] = quantity
        else:
            print('Product not found.')
    elif action == 'r':
        id = input('Enter product ID to remove: ')
        if id in inventory:
            del inventory[id]
        else:
            print('Product not found.')
    elif action == 's':
        for id, quantity in inventory.items():
            print(f'Product ID: {id}, Quantity: {quantity}')