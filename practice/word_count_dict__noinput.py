def count_words(text):
    words = [word.strip().lower() for word in text.split() if word.isalnum()]
    word_counts = {}
    for word in words:
        #region TODO replace this block by shorter block2 below
        word_counts[word] = 1 if word not in word_counts \
                              else word_counts[word]+1
        #endregion TODO replace this block by shorter block2 below

        #region block2
        # word_counts[word] = 1 if   word not in word_counts \
        #                       else word_counts[word]+1
        #endregion block2
    return word_counts

text = 'some text entered directly here ie no input / noinput'
word_counts = count_words(text)
print('Word frequencies:')
for word, count in word_counts.items():
    print(f'{word}: {count}')
