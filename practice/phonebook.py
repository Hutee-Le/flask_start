phonebook = {}
while True:
    name = input('Enter name (or \'q\' to quict): ')
    if name.lower() == 'q':
        break
    number = input('Enter phone number: ')
    phonebook[name] = number

print('Phonebook:')
for name, number in phonebook.items():
    print(f'{name}: {number}')
