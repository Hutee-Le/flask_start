def count_words(text):
    words = [word.strip().lower() for word in text.split() if word.isalnum()]
    word_counts = {}
    for word in words:
        word_counts[word] = 1 if    word not in word_counts \
                              else  word_counts[word]+1
    return word_counts

# text = input("Enter some text: ")
# word_counts = count_words(text)
# print("Word frequencies:")
# for word, count in word_counts.items():
#     print(f"{word}: {count}")
#TODO Nhut try use single quote ' instead of double quote '
text = 'some text in here'
word_counts = count_words(text)
print('Word frequencies:')
for word, count in word_counts.items():
    print(f'{word}: {count}')