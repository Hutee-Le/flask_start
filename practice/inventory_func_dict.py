inventory = {}

# add item in inventory
def add_item(inventory, item_name, quantity, price):
    if item_name in inventory:
        inventory[item_name]['quantity'] += quantity
    else:
        inventory[item_name] = {'quantity': quantity, 'price': price}

# remove item in inventory
def remove_item(inventory, item_name, quantity):
    if item_name in inventory and inventory[item_name]['quantity']>= quantity:
        inventory[item_name]['quantity'] -= quantity
    else:
        print(f'Error: Item {item_name} not found or insufficient quantity.')

# print list all item
def print_inventory(inventory):
    print('Inventory:')
    for item_name, item_details in inventory.items():
        quantity = item_details['quantity']
        price = item_details['price']
        total_price = quantity * price
        print(f'- {item_name}: Quantity: {quantity}, Price: {price:.2f}, Total: {total_price:.2f}')

add_item(inventory, 'Apple', 10, 1.25)
add_item(inventory, 'Bananas', 5, 0.75)
add_item(inventory, 'Oranges', 8, 2.00)

remove_item(inventory, 'Apple', 3)
remove_item(inventory, 'Bananas', 10)

print_inventory(inventory)