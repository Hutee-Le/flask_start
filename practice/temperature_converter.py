def convert_temperature(temperature, scale_from, scale_to):
    if scale_from == 'C' and scale_to == 'F':
        return (temperature * 9/5) + 32
    elif scale_from == 'F' and scale_to == 'C':
        return (temperature - 32) * 5/9
    else:
        print('Invalid conversion scales. Supported scales are \'C\' (Celsius) and \'F\' (Fahrenheit).')
        return None

converted_temp = convert_temperature(0, 'C', 'F')
if converted_temp is not None:
  print(f'0C is equal to {converted_temp:.2f}F')