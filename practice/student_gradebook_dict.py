# grades = {}
#
# while True:
#     action = input("Enter (a)dd, (e)nter grades, (c)alculate average, (s)how all, or (q)uit: ").lower()
#
#     if action == 'q':
#         break
#
#     if action == 'a':
#         name = input("Enter student name: ")
#         grades[name] = {}
#     elif action == 'e':
#         name = input("Enter student name: ")
#         if name in grades:
#             subject = input("Enter subject: ")
#             grade = int(input("Enter grade: "))
#             grades[name][subject] = grade
#         else:
#             print("Student not found.")
#     elif action == 'c':
#         name = input("Enter student name: ")
#         if name in grades:
#             total_grade = 0
#             num_subjects = len(grades[name])
#             for subject, grade in grades[name].items():
#                 total_grade += grade
#             average_grade = total_grade / num_subjects
#             print(f"Average grade for {name}: {average_grade}")
#         else:
#             print("Student not found.")
#     elif action == 's':
#         print("Gradebook:")
#         for name, subjects in grades.items():
#             print(f"Student: {name}")
#             for subject, grade in subjects.items():
#                 print(f"Subject: {subject}, Grade: {grade}")
#     else:
#         print("Invalid action. Please try again.")
#TODO Nhut try use single quote ' instead of double quote "
grades = {}

while True:
    action = input('Enter (a)dd, (e)nter grades, (c)alculate average, (s)how all, or (q)uit: ').lower()

    if action == 'q':
        break

    if action == 'a':
        name = input('Enter student name: ')
        grades[name] = {}
    elif action == 'e':
        name = input('Enter student name: ')
        if name in grades:
            subject = input('Enter subject: ')
            grade = int(input('Enter grade: '))
            grades[name][subject] = grade
        else:
            print('Student not found.')
    elif action == 'c':
        name = input('Enter student name: ')
        if name in grades:
            total_grade = 0
            num_subjects = len(grades[name])
            for subject, grade in grades[name].items():
                total_grade += grade
            average_grade = total_grade / num_subjects
            print(f'Average grade for {name}: {average_grade}')
        else:
            print('Student not found.')
    elif action == 's':
        print('Gradebook:')
        for name, subjects in grades.items():
            print(f'Student: {name}')
            for subject, grade in subjects.items():
                print(f'Subject: {subject}, Grade: {grade}')
    else:
        print('Invalid action. Please try again.')