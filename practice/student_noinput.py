grades = {
    'Nut': {'English': 6, 'Math': 7.5, 'Science': 7},
    'Kan': {'English': 6, 'Math': 7.5, 'Science': 7},
    'Elise': {'English': 6, 'Math': 7.5, 'Science': 7},
}


def calculate_average(name):
    if name not in grades:
        print(f'Student {name} not found')
        return

    total_grade = 0
    num_subjects = len(grades[name])
    for subject, grade in grades[name].items():
        total_grade += grade
    average_grade = total_grade / num_subjects
    print(f'Average grade for {name}: {average_grade:.2f}')


def show_all_grades():
    print('Gradebook:')
    for name, subject in grades.items():
        print(f'Student: {name}')
        for subject, grade in subject.items():
            print(f'Subject: {subject}, Grade: {grade}')


calculate_average("Nut")
show_all_grades()
